package isometric.app;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;

import isometric.chunk.Chunk;
import isometric.chunk.ChunkManager;
import isometric.entities.EntityPropertiesManager;
import isometric.terrain.TerrainCellImageManager;
import isometric.world.WorldController;
import isometric.world.WorldData;
import isometric.world.WorldProperties;
import isometric.world.WorldView;
import isometric.world.WorldWindow;

public class IsometricDemo {

	private WorldProperties worldProperties;
	private EntityPropertiesManager entityPropertiesManager;
	private TerrainCellImageManager cellImages;
	private ChunkManager chunkManager;
	private WorldView worldView;
	private WorldWindow worldWindow;
	private WorldController worldController;
	private WorldData worldData;
	private JFrame w;

	private static int universeNumber = 0;

	public static void main(String[] args) {
		universeNumber = 0;
		// for (universeNumber = 0; universeNumber < 10; universeNumber++) {
		IsometricDemo demo = new IsometricDemo();
		demo.showWorldWindow();
		demo.initialise();
		// demo.generateChunks();
		demo.generateWorldView();
		demo.generateWorldController();
		demo.draw();
		System.out.println("Drawn");
		// }
	}

	private void showWorldWindow() {
		this.w = new JFrame();
		this.w.setLocationByPlatform(true);
		Dimension size = new Dimension(1024, 900);
		this.worldWindow.setPreferredSize(size);
		this.worldWindow.setSize(size);
		this.worldProperties.setWidthHeight(this.worldWindow);
		this.w.setContentPane(this.worldWindow);
		this.w.setTitle("Isometric " + universeNumber);
		this.w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.w.pack();
		this.w.setVisible(true);
		this.worldWindow.prepareToDraw();
	}

	private void initialise() {
		this.cellImages = new TerrainCellImageManager(this.worldProperties);
		this.chunkManager = new ChunkManager(this.worldProperties, this.worldData);
	}

	public IsometricDemo() {
		this.worldProperties = new WorldProperties();
		this.worldWindow = new WorldWindow();
		this.entityPropertiesManager = new EntityPropertiesManager(this.worldProperties);
		this.entityPropertiesManager.generateDefaultEntities();
		this.worldData = new WorldData(this.worldProperties, this.entityPropertiesManager);
		this.worldWindow.addComponentListener(new ComponentListener() {
			@Override
			public void componentShown(ComponentEvent e) {
			}

			@Override
			public void componentResized(ComponentEvent e) {
				if (worldView != null) {
					worldProperties.setWidthHeight(worldWindow);
					worldWindow.prepareToDraw();
					worldView.visualiseNearbyWorld();
				}
			}

			@Override
			public void componentMoved(ComponentEvent e) {
			}

			@Override
			public void componentHidden(ComponentEvent e) {
			}
		});
	}

	public void flatWorld() {
		Chunk chunk = new Chunk(this.worldProperties);
		int end = this.worldProperties.chunkSize - 1;
		chunk.setHeights(0, 0, end, end, 0);
		this.chunkManager = new ChunkManager(this.worldProperties, this.worldData);
		this.chunkManager.addChunk(chunk);
	}

	public void generateChunks() {
		this.chunkManager = new ChunkManager(this.worldProperties, this.worldData);
		this.worldChunk(0, 0);
		this.worldChunk(1, 0);
		this.worldChunk(-1, 0);
		this.worldChunk(0, 1);
		this.worldChunk(0, -1);
		this.worldChunk(1, 1);
		this.worldChunk(1, -1);
		this.worldChunk(-1, 1);
		this.worldChunk(-1, -1);
	}

	private void worldChunk(int chunkCellX, int chunkCellY) {
		Chunk chunk = new Chunk(this.worldProperties);
		chunk.setChunkGridPosition(chunkCellX, chunkCellY);
		chunk.setFromWorldData(this.worldData);
		this.chunkManager.addChunk(chunk);
	}

	private void generateWorldView() {
		this.worldView = new WorldView(this.worldProperties, this.cellImages, this.chunkManager, this.worldWindow);
		this.worldView.generateChunkViews();
	}

	private void generateWorldController() {
		this.worldController = new WorldController(this.worldView);
		this.w.addKeyListener(this.worldController);
	}

	private void draw() {
		// this.worldView.visualiseNearbyWorld();
		// Runnable uiUpdate = new Runnable() {
		// @Override
		// public void run() {
		// worldWindow.updateUI();
		// SwingUtilities.invokeLater(this);
		// }
		// };
		// SwingUtilities.invokeLater(uiUpdate);
	}
}
