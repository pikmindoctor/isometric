package isometric.world;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeMap;

import isometric.chunk.Chunk;
import isometric.chunk.ChunkManager;
import isometric.chunk.ChunkView;
import isometric.sidesAndVectors.CellPosition;
import isometric.sidesAndVectors.Vector2;
import isometric.terrain.TerrainCellImageManager;

public class WorldView {
	private ChunkManager chunkManager;
	private WorldProperties worldProperties;
	private TerrainCellImageManager cellImages;
	private TreeMap<CellPosition, ChunkView> chunkViews = new TreeMap<CellPosition, ChunkView>();
	private WorldWindow worldWindow;
	private Vector2 cameraPosition = new Vector2(0, 0);

	public WorldView(WorldProperties worldProperties, TerrainCellImageManager cellImages, ChunkManager chunkManager,
			WorldWindow worldWindow) {
		this.worldProperties = worldProperties;
		this.cellImages = cellImages;
		this.chunkManager = chunkManager;
		this.worldWindow = worldWindow;
	}

	public void generateChunkViews() {
		this.chunkViews.clear();
		ChunkView chunkView;
		for (Chunk chunk : this.chunkManager.getChunks()) {
			if (!this.chunkViews.containsKey(chunk.getCellPosition())) {
				chunkView = new ChunkView(this.worldProperties, this.cellImages, chunk);
				this.chunkViews.put(chunk.getCellPosition(), chunkView);
			}
		}
	}

	public void visualiseNearbyWorld() {
		ChunkView chunkView;
		HashSet<CellPosition> nearbyCellPositions = this.chunkManager.nearbyCellPositionsSet(this.cameraPosition);
		int size = this.chunkViews.size();
		if (size > nearbyCellPositions.size() * 2) {
			System.out.println("Remove");
			ArrayList<CellPosition> toRemove = new ArrayList<CellPosition>(this.chunkViews.size());
			this.chunkViews.forEach((key, value) -> {
				if (!nearbyCellPositions.contains(key)) {
					toRemove.add(key);
				}
			});
			for (CellPosition cellPosition : toRemove) {
				this.chunkViews.remove(cellPosition);
				this.chunkManager.forgetChunk(cellPosition);
				size = this.chunkViews.size();
			}
		}
		for (Chunk chunk : this.chunkManager.generateNearbyChunks(this.cameraPosition)) {
			if (!this.chunkViews.containsKey(chunk.getCellPosition())) {
				chunkView = new ChunkView(this.worldProperties, this.cellImages, chunk);
				this.chunkViews.put(chunkView.getCellPosition(), chunkView);
			}
		}
		this.drawChunkViews();
	}

	public WorldWindow getWorldWindow() {
		return this.worldWindow;
	}

	public void moveCamera(double x, double y) {
		this.cameraPosition.x += x;
		this.cameraPosition.y += y;
	}

	public void drawChunkViews() {
		this.worldWindow.clear();
		for (ChunkView chunkView : this.chunkViews.values()) {
			this.worldWindow.drawChunk(chunkView, this.cameraPosition);
		}
	}
}
