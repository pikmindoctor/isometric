package isometric.world;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import isometric.chunk.ChunkView;
import isometric.sidesAndVectors.Vector2;

public class WorldWindow extends JPanel {
	private static final long serialVersionUID = -2116000012565968050L;
	private BufferedImage img;

	public void prepareToDraw() {
		int width = this.getWidth();
		int height = this.getHeight();
		this.img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	}

	public void drawChunk(ChunkView chunkView, Vector2 cameraPosition) {
		if (!chunkView.getDrawStarted()) {
			chunkView.drawLater();
		}
		if (chunkView.getDrawn()) {
			Vector2 pixelPosition = chunkView.getPixelPosition();
			this.img.getGraphics().drawImage(chunkView.getImage(), (int) (cameraPosition.x + pixelPosition.x),
					(int) (cameraPosition.y + pixelPosition.y), this);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(this.img, 0, 0, this);
	}

	public void clear() {
		this.img.getGraphics().clearRect(0, 0, this.img.getWidth(), this.img.getHeight());
	}
}
