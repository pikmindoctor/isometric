package isometric.world;

import java.util.ArrayList;
import java.util.Random;

import isometric.sidesAndVectors.CellPosition;

public class WorldProperties {
	public int chunkSize = 8;
	public int tileScale = 1;
	public int maximumHeight = 128 * this.tileScale;
	public int tilePixelWidth = 64 * this.tileScale;
	public int tileFlatPixelHeight = 32 * this.tileScale;
	public int tileElevatedPixelHeight = 48 * this.tileScale;
	public int maxElevationHeightScale = 2;
	public int layerGap = 0 * this.tileScale;
	public int chunkMargin = 1;
	public int windowWidth;
	public int windowHeight;
	public int nLayers = 4;
	public Random random = new Random(0);
	public boolean concurrentChunkDrawing = true;

	public CellPosition getCellPosition(double pixelX, double pixelY) {
		int wo2 = (int) (this.tilePixelWidth * chunkSize * 0.5);
		int ho2 = (int) (this.tileFlatPixelHeight * this.chunkSize * 0.5);
		pixelX -= wo2;
		pixelY -= this.tileFlatPixelHeight * this.chunkSize;
		double cellX = -0.5 * pixelX / wo2 - 0.5 * pixelY / ho2;
		double cellY = -0.5 * pixelX / wo2 + 0.5 * pixelY / ho2;
		// Vector2 pixelPosition = new Vector2(wo2 * cellX + 2 * ho2 * cellY, wo2 *
		// cellX / 2 - ho2 * cellY);
		// System.out.println(pixelPosition.x + "," + pixelPosition.y);
		return new CellPosition((int) cellX, (int) cellY);
	}

	public ArrayList<CellPosition> nearbyCellPositions(double pixelX, double pixelY) {
		ArrayList<CellPosition> cellPositions = new ArrayList<CellPosition>();
		int w = this.windowWidth;
		int h = this.windowHeight;
		if (w <= 0 || h <= 0) {
			return null;
		}
		int gapX = w / 16;
		int gapY = h / 16;
		for (int y = -h; y <= h * 2; y += gapY) {
			for (int x = -w; x <= w * 2; x += gapX) {
				cellPositions.add(this.getCellPosition(x + pixelX, y + pixelY));
			}
		}
		return cellPositions;
	}

	public void setWidthHeight(WorldWindow worldWindow) {
		this.windowWidth = worldWindow.getWidth();
		this.windowHeight = worldWindow.getHeight();
	}

	public int random(int nTreeOffsetVariations) {
		return this.random.nextInt(nTreeOffsetVariations);
	}
}
