package isometric.world;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import isometric.entities.EntityProperties;
import isometric.entities.EntityPropertiesManager;

public class WorldData {
	private WorldProperties worldProperties;
	private HashMap<String, Integer> randomNumbers = new HashMap<String, Integer>();
	private EntityPropertiesManager entityPropertiesManager;

	public WorldData(WorldProperties worldProperties, EntityPropertiesManager entityPropertiesManager) {
		this.worldProperties = worldProperties;
		this.entityPropertiesManager = entityPropertiesManager;
	}

	public BigInteger md5(String message) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.update(message.getBytes(), 0, message.length());
			// return new BigInteger(1, digest.digest()).toString(16);
			return new BigInteger(1, digest.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getRandom(int x, int y, int max) {
		int keyNumber;
		int result;
		String key = x + "," + y;
		Integer a = this.randomNumbers.get(key);
		if (a == null) {
			// keyNumber = ("�$" + key + "&").hashCode();
			keyNumber = Math.abs(md5(key).intValue());
			// keyNumber = key.hashCode();
			result = (keyNumber) % (max + 1);
			this.randomNumbers.put(key, result);
			return result;
		} else {
			return a.intValue();
		}
	};

	public int getHeight(int x, int y) {
		int maxLevel = this.worldProperties.nLayers;
		int levelFound = 0;
		for (int level = 0; level <= maxLevel; level++) {
			levelFound = level;
			if (!this.getIsAtOrUnder(x, y, level)) {
				break;
			}
		}
		return levelFound;
	}

	public EntityProperties getEntityAt(int x, int y) {
		int nTreeOffsetVariations = this.entityPropertiesManager.getNTreeOffsetVariations();
		if (this.getRandom(x, y, 2) == 0) {
			int random = this.worldProperties.random(nTreeOffsetVariations);
			return this.entityPropertiesManager.getEntityProperties("Tree" + random);
		} else {
			return null;
		}
	}

	public int sumRandom(int x, int y, int layer, int range) {
		if (layer == 0) {
			int r = this.getRandom(x, y, range);
			return r;
		} else {
			String key = "sr" + layer + ":" + x + "," + y;
			Integer result = this.randomNumbers.get(key);
			int isum;
			int layerMinus1 = layer - 1;
			double sum;
			int a = 16;
			int spread = 1;
			if (result == null) {
				sum = this.sumRandom(x, y, layerMinus1, a);
				sum += this.sumRandom(x + spread, y, layerMinus1, a);
				sum += this.sumRandom(x + spread, y - spread, layerMinus1, a);
				sum += this.sumRandom(x, y - spread, layerMinus1, a);
				sum += this.sumRandom(x - spread, y - spread, layerMinus1, a);
				sum += this.sumRandom(x - spread, y, layerMinus1, a);
				sum += this.sumRandom(x - spread, y + spread, layerMinus1, a);
				sum += this.sumRandom(x, y + spread, layerMinus1, a);
				sum += this.sumRandom(x + spread, y, layerMinus1, a);
				// sum *= range;
				sum /= 9 * a;
				sum -= 0.5;
				sum *= 1.0 + 0.2 * layer;
				sum += 0.5;
				sum *= range;
				isum = (int) sum;
				this.randomNumbers.put(key, isum);
				return isum;
			} else {
				return result.intValue();
			}
		}
	}

	public boolean getIsAtOrUnder(int x, int y, int level) {
		int range = 32;
		int r = this.sumRandom(x, y, 3, range);
		if (level == 0)
			return r < 24;
		else
			return r < 8 && this.isFlatArea(x, y, level - 1);
		// } else {
		// return this.sumRandom(x, y, 4, range) < 1 && this.isFlatArea(x, y, level -
		// 1);
		// }
	}

	private boolean isFlatArea(int x, int y, int level) {
		String key = "fa" + level + ":" + x + "," + y;
		Integer result = this.randomNumbers.get(key);
		boolean b;
		if (result == null) {
			b = this.getIsAtOrUnder(x, y, level) && this.getIsAtOrUnder(x + 1, y, level)
					&& this.getIsAtOrUnder(x + 1, y - 1, level) && this.getIsAtOrUnder(x, y - 1, level)
					&& this.getIsAtOrUnder(x - 1, y - 1, level) && this.getIsAtOrUnder(x - 1, y, level)
					&& this.getIsAtOrUnder(x - 1, y + 1, level) && this.getIsAtOrUnder(x, y + 1, level)
					&& this.getIsAtOrUnder(x + 1, y + 1, level);
			this.randomNumbers.put(key, b ? 1 : 0);
			return b;
		} else {
			return result == 1;
		}
	}

	public void clearCache() {
		this.randomNumbers.clear();
	}
}
