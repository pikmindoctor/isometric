package isometric.world;

import java.awt.event.KeyEvent;
import java.util.LinkedHashSet;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.SwingUtilities;

import isometric.util.IsometricUtil;

public class WorldController implements java.awt.event.KeyListener {

	private WorldView worldView;
	private LinkedHashSet<Integer> keys;
	private ConcurrentLinkedQueue<Runnable> swingTasks = new ConcurrentLinkedQueue<Runnable>();
	private double dx;
	private double dy;
	private double currentTime;
	private double prevTime;
	private double dt;

	public WorldController(WorldView worldView) {
		this.worldView = worldView;
		this.keys = new LinkedHashSet<Integer>();
		this.swingTasks = new ConcurrentLinkedQueue<Runnable>();
		this.swingTasks().start();
	}

	public Thread swingTasks() {
		return new Thread(new Runnable() {
			@Override
			public void run() {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						while (!swingTasks.isEmpty()) {
							swingTasks.poll().run();
						}
						worldView.visualiseNearbyWorld();
						worldView.getWorldWindow().repaint();
						currentTime = IsometricUtil.levelTime();
						if (prevTime != 0) {
							dt = currentTime - prevTime;
						}
						prevTime = currentTime;
						update();
						swingTasks().start();
					}
				});
			}
		});
	}

	@Override
	public void keyPressed(KeyEvent ke) {
		int code = ke.getKeyCode();
		this.keys.add(code);
	}

	public void update() {
		double amount = 320.0 * dt;
		dx = dy = 0;
		for (int code : this.keys) {
			switch (code) {
			case KeyEvent.VK_D:
				this.dx -= amount * 2;
				break;
			case KeyEvent.VK_W:
				this.dy += amount;
				break;
			case KeyEvent.VK_A:
				this.dx += amount * 2;
				break;
			case KeyEvent.VK_S:
				this.dy -= amount;
				break;
			}
		}
		this.swingTasks.add(new Runnable() {
			@Override
			public void run() {
				worldView.moveCamera(dx, dy);
			}
		});
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		int code = ke.getKeyCode();
		keys.remove(code);
	}

	@Override
	public void keyTyped(KeyEvent ke) {
	}

}