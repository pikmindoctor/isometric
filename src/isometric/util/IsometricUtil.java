package isometric.util;

import java.awt.Color;

public class IsometricUtil {

	public static double SmootherStep(double t) {
		return t = t * t * t * (t * (6f * t - 15f) + 10f);
	}

	public static int blendColours(int[] colours, double[] amounts) {
		double[] rgbTotals = new double[3];
		int n = colours.length;
		int colour;
		for (int a = 0; a < n; a++) {
			colour = colours[a];
			rgbTotals[0] += (colour & 0x00FF0000) >> 16;
			rgbTotals[1] += (colour & 0x0000FF00) >> 8;
			rgbTotals[2] += (colour & 0x000000FF);
		}
		rgbTotals[0] /= n;
		rgbTotals[1] /= n;
		rgbTotals[2] /= n;
		int r = (int) rgbTotals[0];
		int g = (int) rgbTotals[1];
		int b = (int) rgbTotals[2];
		return new Color(r, g, b).getRGB();
		// return Color.red.getRGB();
	}

	public static int adjustLightness(int colour, double lightness) {
		int r = (int) (((colour & 0x00FF0000) >> 16) * lightness);
		int g = (int) (((colour & 0x0000FF00) >> 8) * lightness);
		int b = (int) ((colour & 0x000000FF) * lightness);
		return new Color(r, g, b).getRGB();
	}

	public static double levelTime() {
		return (double) (System.currentTimeMillis()) / 1000.0;
	}
}
