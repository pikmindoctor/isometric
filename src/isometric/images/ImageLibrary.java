package isometric.images;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLibrary {
	public static BufferedImage getImage(String name, boolean transparent) {
		try {
			Image img = ImageIO.read(ImageLibrary.class.getResource(name));
			int width = img.getWidth(null);
			int height = img.getHeight(null);
			BufferedImage bimg = new BufferedImage(width, height,
					transparent ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);
			bimg.getGraphics().drawImage(img, 0, 0, null);
			return bimg;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
