package isometric.terrain;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class TerrainMixture {
	private HashMap<TerrainType, Double> map = new HashMap<TerrainType, Double>();

	public void add(TerrainType terrainType, double amount) {
		this.map.put(terrainType, amount);
	}

	public Set<TerrainType> getTerrainTypes() {
		return this.map.keySet();
	}

	public void normalise() {
		double total = 0;
		for (Entry<TerrainType, Double> entry : this.map.entrySet()) {
			total += entry.getValue();
		}
		for (Entry<TerrainType, Double> entry : this.map.entrySet()) {
			entry.setValue(entry.getValue() / total);
		}
	}

	public double getAmount(TerrainType terrainType) {
		return this.map.get(terrainType);
	}
}
