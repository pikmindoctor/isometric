package isometric.terrain;

import java.util.Arrays;

import isometric.sidesAndVectors.Side;

public class CellTerrainProperties {
	public static final int N = 8;
	public static final CellTerrainProperties air = new CellTerrainProperties(TerrainType.air);
	private int[] levels = new int[N];
	int middleLevel = 0;
	private TerrainType[] terrainTypes = new TerrainType[N + 1];
	public boolean transparentFloor = false;

	public CellTerrainProperties(boolean transparentFloor) {
		this.transparentFloor = transparentFloor;
		for (int a = 0; a < N + 1; a++) {
			this.terrainTypes[a] = TerrainType.unassigned;
		}
	}

	public CellTerrainProperties(TerrainType terrainType) {
		for (int a = 0; a < N + 1; a++) {
			this.terrainTypes[a] = terrainType;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(levels);
		result = prime * result + middleLevel > 0 ? 1 : 0;
		result = prime * result + Arrays.hashCode(terrainTypes);
		result = prime * result + (transparentFloor ? 1 : 0);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CellTerrainProperties other = (CellTerrainProperties) obj;
		if (!Arrays.equals(levels, other.levels))
			return false;
		if (middleLevel != other.middleLevel)
			return false;
		if (!Arrays.equals(terrainTypes, other.terrainTypes))
			return false;
		if (transparentFloor != other.transparentFloor)
			return false;
		return true;
	}

	public void setMiddleLevel(int middleLevel) {
		if (middleLevel < 0) {
			middleLevel = 0;
		}
		this.middleLevel = middleLevel;
	}

	public void setLevel(Side side, int level) {
		int a0 = side.ordinal();
		this.levels[a0] = level;
	}

	public static double SmootherFrac(double frac) {
		double t = frac > 0 ? frac : -frac;
		t = t * t * t * (t * (6f * t - 15f) + 10f);
		return frac > 0 ? t : -t;
	}

	public static double SmoothFrac(double frac) {
		double t = frac > 0 ? frac : -frac;
		// t = t * t * t * (t * (6f * t - 15f) + 10f) - 0.01;
		t = t * t * (3.0 - 2.0 * t) - 0.025;
		if (t < 0.0) {
			t = 0.0;
		}
		return frac > 0 ? t : -t;
	}

	// 0: east
	// 1: north-east
	// 2: north
	// 3: north-west
	// 4: west
	// 5: south-west
	// 6: south
	// 7: south-east

	// 3 < 2 < 1
	// v ##### ^
	// 4 ##### 0
	// v ##### ^
	// 5 > 6 > 7

	public double getHeight(double fracX, double fracY) {
		if (this.middleLevel == 1) {
			return -1;
		}
		double total = 0;
		for (int a = 0; a < N; a++) {
			total += this.levels[a];
		}
		if (total == 0) {
			return 0;
		}
		double x = (0.5 - fracX) * 2;
		double y = (0.5 - fracY) * 2;
		x = SmoothFrac(x);
		y = SmoothFrac(y);
		double ns = -this.lerpHeight(this.levels[0], this.levels[4], x);
		double ew = -this.lerpHeight(this.levels[2], this.levels[6], -y);
		double perpendicular = Math.min(ns, ew);
		double nwse = -this.lerpHeight(this.levels[3], this.levels[7], -diagonalPosition(x, y, true));
		double nesw = -this.lerpHeight(this.levels[1], this.levels[5], -diagonalPosition(x, y, false));
		double diagonal = Math.min(nwse, nesw);
		return Math.min(perpendicular, diagonal);
	}

	public TerrainMixture getTerrainMixture(double fracX, double fracY) {
		TerrainMixture terrainMixture = new TerrainMixture();
		TerrainType centre = this.terrainTypes[N];
		terrainMixture.add(centre, 1.0);
		return terrainMixture;

	}

	private double diagonalPosition(double x, double y, boolean north) {
		double angle = north ? -Math.PI / 4.0 : Math.PI / 4.0;
		double extent = (Math.cos(angle + Math.atan2(y, x)) * Math.sqrt(x * x + y * y)) / 1.414;
		extent = north ? extent : -extent;
		double absExtent = Math.abs(extent);
		if (Math.abs(absExtent) > 0.5) {
			if (extent > 0) {
				return (extent - 0.5) * 2;
			} else {
				return (extent + 0.5) * 2;
			}
		} else {
			return 0;
		}
	}

	public double lerp2(double a, double b, double t) {
		double range = b - a;
		return a + range * t;
	}

	public double lerpHeight(double a, double b, double t) {
		if (t > 0.0) {
			return this.lerp2(0, b, t);
		} else {
			return this.lerp2(a, 0, t + 1);
		}
	}

	public double getLightness(double u, double v, double height) {
		double slope = height + this.getHeight(u - 0.01, v - 0.01);
		double shadow = slope * 10 + 0.5;
		if (shadow > 1.0) {
			shadow = 1.0;
		} else if (shadow < 0.0) {
			shadow = 0.0;
		}
		return 1.0 - shadow * 0.5;
	}
}
