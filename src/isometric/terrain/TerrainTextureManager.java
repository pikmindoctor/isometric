package isometric.terrain;

import java.util.HashMap;

import isometric.images.ImageLibrary;

public class TerrainTextureManager {

	HashMap<String, TerrainTexture> cache = new HashMap<String, TerrainTexture>();

	public TerrainTexture getTerrainTexture(String name) {
		TerrainTexture terrainTexture = this.cache.get(name);
		if (terrainTexture == null) {
			if ("unassigned".equals(name)) {
				terrainTexture = new TerrainTexture(ImageLibrary.getImage("Grass.png", false));
			}
			this.cache.put(name, terrainTexture);
		}
		return terrainTexture;
	}
}
