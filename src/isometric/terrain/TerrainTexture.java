package isometric.terrain;

import java.awt.image.BufferedImage;

public class TerrainTexture {
	private int[] albedo;
	private int width;
	private int height;

	public TerrainTexture(BufferedImage albedoImage) {
		this.width = albedoImage.getWidth();
		this.height = albedoImage.getHeight();
		this.albedo = getRGBArray(albedoImage);
	}

	private int[] getRGBArray(BufferedImage img) {
		return img.getRGB(0, 0, this.width, this.height, this.albedo, 0, this.width);
	}

	public int getPixel(double u, double v) {
		int x = (int) (this.width * u);
		int y = (int) (this.height * v);
		return this.albedo[y * this.width + x];
	}
}
