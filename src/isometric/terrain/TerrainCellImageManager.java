package isometric.terrain;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import isometric.util.IsometricUtil;
import isometric.world.WorldProperties;

public class TerrainCellImageManager {

	public ConcurrentHashMap<CellTerrainProperties, BufferedImage> cache = new ConcurrentHashMap<CellTerrainProperties, BufferedImage>();
	private WorldProperties worldProperties;
	public TerrainTextureManager terrainTextureManager = new TerrainTextureManager();
	private int water = new Color(104, 151, 226).getRGB();
	private static int transparent = new Color(0, 0, 0, 0).getRGB();

	public TerrainCellImageManager(WorldProperties worldProperties) {
		this.worldProperties = worldProperties;
	}

	public BufferedImage getCellTerrain(CellTerrainProperties cellTerrainProperties) {
		BufferedImage img = this.cache.get(cellTerrainProperties);
		if (img != null) {
			return img;
		} else {
			int width = this.worldProperties.tilePixelWidth;
			int height = this.worldProperties.tileElevatedPixelHeight;
			img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			this.generateCellTerrain(cellTerrainProperties, img, width, height);
			this.cache.put(cellTerrainProperties, img);
			return img;
		}
	}

	private void generateCellTerrain(CellTerrainProperties cellTerrainProperties, BufferedImage img, int width,
			int height) {
		int[] pixels = new int[this.worldProperties.tilePixelWidth * this.worldProperties.tileElevatedPixelHeight];
		Graphics2D g2d = (Graphics2D) img.getGraphics();
		g2d.setColor(Color.white);
		double elevation;
		int size = width;
		double middleX = size / 2.0;
		double maxElevationHeight = this.worldProperties.maxElevationHeightScale * this.worldProperties.tileScale;
		double middleBottomY = this.worldProperties.tileElevatedPixelHeight - maxElevationHeight;
		int targetX;
		int targetY;
		int color = Color.WHITE.getRGB();
		int nPixels = pixels.length;
		int index;
		double u;
		double v;
		for (int a = 0; a < nPixels; a++) {
			pixels[a] = 0;
		}
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				u = ((double) x) / size;
				v = ((double) y) / size;
				elevation = cellTerrainProperties.getHeight(u, v);
				targetX = (int) (middleX - 1 - x / 2.0 + y / 2.0);
				targetY = (int) (middleBottomY - 1 - x / 4.0 - y / 4.0 + elevation * maxElevationHeight);
				index = targetY * width + targetX;
				if (elevation >= 0.0) {
					if (cellTerrainProperties.transparentFloor) {
						pixels[index] = transparent;
					} else {
						pixels[index] = water;
					}
					continue;
				}
				color = this.sampleFromTexture(u, v, cellTerrainProperties);
				color = IsometricUtil.adjustLightness(color, cellTerrainProperties.getLightness(u, v, -elevation));
				if (index >= 0 && index < pixels.length) {
					pixels[index] = color;
				}
			}
		}
		img.setRGB(0, 0, width, height, pixels, 0, width);
	}

	private int sampleFromTexture(double u, double v, CellTerrainProperties cellTerrainProperties) {
		TerrainMixture terrainMixture = cellTerrainProperties.getTerrainMixture(u, v);
		TerrainTexture texture;
		Set<TerrainType> terrainTypes = terrainMixture.getTerrainTypes();
		int nTypes = terrainTypes.size();
		int[] colours = new int[nTypes];
		double[] amounts = new double[nTypes];
		int index = 0;
		for (TerrainType terrainType : terrainTypes) {
			texture = this.terrainTextureManager.getTerrainTexture(terrainType.name());
			if (texture == null) {
				colours[index] = transparent;
			} else {
				colours[index] = texture.getPixel(u, v);
			}
			amounts[index] = terrainMixture.getAmount(terrainType);
			index++;
		}
		return IsometricUtil.blendColours(colours, amounts);
	}
}
