package isometric.chunk;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import isometric.sidesAndVectors.CellPosition;
import isometric.sidesAndVectors.Vector2;
import isometric.world.WorldData;
import isometric.world.WorldProperties;

public class ChunkManager {
	private ArrayList<Chunk> chunks = new ArrayList<Chunk>();
	private HashMap<String, Chunk> chunkMap = new HashMap<String, Chunk>();
	private WorldData worldData;
	private WorldProperties worldProperties;

	public ChunkManager(WorldProperties worldProperties, WorldData worldData) {
		this.worldProperties = worldProperties;
		this.worldData = worldData;
	}

	public Collection<Chunk> getChunks() {
		return this.chunks;
	}

	public ArrayList<CellPosition> nearbyCellPositions(Vector2 cameraPosition) {
		return this.worldProperties.nearbyCellPositions(cameraPosition.x, cameraPosition.y);
	}

	public HashSet<CellPosition> nearbyCellPositionsSet(Vector2 cameraPosition) {
		HashSet<CellPosition> cellPositions = new HashSet<CellPosition>();
		cellPositions.addAll(this.nearbyCellPositions(cameraPosition));
		return cellPositions;
	}

	public ArrayList<Chunk> generateNearbyChunks(Vector2 cameraPosition) {
		ArrayList<CellPosition> nearbyCellPositions = this.nearbyCellPositions(cameraPosition);
		ArrayList<Chunk> generatedChunks = new ArrayList<Chunk>();
		for (CellPosition cellPosition : nearbyCellPositions) {
			if (!this.chunkAlreadyGenerated(cellPosition)) {
				generatedChunks.add(this.worldChunk(cellPosition.x, cellPosition.y));
			}
		}
		return generatedChunks;
	}

	private boolean chunkAlreadyGenerated(CellPosition cellPosition) {
		return this.chunkMap.containsKey(cellPosition.toString());
	}

	private Chunk worldChunk(int chunkCellX, int chunkCellY) {
		Chunk chunk = new Chunk(this.worldProperties);
		chunk.setChunkGridPosition(chunkCellX, chunkCellY);
		chunk.setFromWorldData(this.worldData);
		this.addChunk(chunk);
		return chunk;
	}

	public void addChunk(Chunk chunk) {
		this.worldProperties = chunk.getWorldProperties();
		this.chunks.add(chunk);
		String key = chunk.getCellPosition().toString();
		this.chunkMap.put(key, chunk);
	}

	public void removeChunk(Chunk chunk) {
		CellPosition position = chunk.getCellPosition();
		String key = position.toString();
		this.chunkMap.remove(key);
		this.chunks.remove(chunk);
	}

	public void removeChunk(CellPosition position) {
		String key = position.toString();
		this.chunks.remove(this.chunkMap.remove(key));
	}

	public void forgetChunk(CellPosition cellPosition) {
		this.removeChunk(cellPosition);
	}
}
