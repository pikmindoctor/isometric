package isometric.chunk;

import isometric.entities.EntityProperties;
import isometric.sidesAndVectors.CellPosition;
import isometric.sidesAndVectors.Side;
import isometric.terrain.CellTerrainProperties;
import isometric.world.WorldData;
import isometric.world.WorldProperties;

public class Chunk implements Comparable<Chunk> {
	private int[][] heights;
	private WorldProperties worldProperties;
	private CellPosition cellPosition = new CellPosition(0, 0);
	private CellPosition worldXY = new CellPosition(0, 0);
	private int sizePlusMargin;
	private int margin;
	private WorldData worldData;

	public Chunk(WorldProperties worldProperties) {
		this.worldProperties = worldProperties;
		this.margin = this.worldProperties.chunkMargin;
		this.sizePlusMargin = this.worldProperties.chunkSize + margin * 2;
		this.heights = new int[this.sizePlusMargin][this.sizePlusMargin];
	}

	public void setHeights(int startX, int startY, int endX, int endY, int height) {
		int[] row;
		for (int y = startY; y <= endY; y++) {
			row = this.heights[y];
			for (int x = startX; x <= endX; x++) {
				row[x] = height;
			}
		}
	}

	public void setHeight(int x, int y, int height) {
		this.heights[y][x] = height;
	}

	public Integer getHeight(int x, int y) {
		return this.heights[y][x];
	}

	public EntityProperties getEntityProperties(int x, int y) {
		return this.worldData.getEntityAt(x, y);
	}

	public void setHeightsPattern(int startX, int startY, int endX, int endY, int... heights) {
		int[] row;
		int a = 0;
		for (int y = startY; y <= endY; y++) {
			row = this.heights[y];
			for (int x = startX; x <= endX; x++) {
				row[x] = heights[a];
				a++;
			}
		}
	}

	public int[] getHeightRow(int y) {
		return this.heights[y];
	}

	public Integer getHeight(int[] xy, int x, int y) {
		x += xy[0];
		y += xy[1];
		if (x < 0) {
			return null;
		}
		if (y < 0) {
			return null;
		}
		int chunkSize = this.worldProperties.chunkSize;
		if (x > chunkSize + this.margin) {
			return null;
		}
		if (y > chunkSize + this.margin) {
			return null;
		}
		return this.heights[y][x];
	}

	private int[] changeBySide(Side side, int x, int y) {
		switch (side) {
		case east:
			return new int[] { 1, 0 };
		case northEast:
			return new int[] { 1, -1 };
		case north:
			return new int[] { 0, -1 };
		case northWest:
			return new int[] { -1, -1 };
		case west:
			return new int[] { -1, 0 };
		case southWest:
			return new int[] { -1, 1 };
		case south:
			return new int[] { 0, 1 };
		case southEast:
			return new int[] { 1, 1 };
		}
		return null;
	}

	public CellTerrainProperties getCellTerrainProperties(int x, int y, int layer, boolean transparentFloor) {
		CellTerrainProperties ctp = new CellTerrainProperties(transparentFloor);
		int N = CellTerrainProperties.N;
		Integer height;
		ctp.setMiddleLevel(this.getHeight(x, y) - layer);
		for (int a = 0; a < N; a++) {
			for (Side side : Side.values()) {
				height = this.getHeight(this.changeBySide(side, x, y), x, y);
				if (height != null) {
					height -= layer;
					if (height < 0) {
						height = 0;
					} else if (height > 1) {
						height = 1;
					}
					ctp.setLevel(side, height);
				}
			}
		}
		return ctp;
	}

	public boolean isSurroundedByHeight(int x, int y, int layer) {
		Integer height;
		int[] change = new int[] { 1, 0 };
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		change[1] = -1;
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		change[0] = 0;
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		change[0] = -1;
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		change[1] = 0;
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		change[1] = 1;
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		change[0] = 0;
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		change[0] = 1;
		if ((height = this.getHeight(change, x, y)) == null || height < layer) {
			return false;
		}
		return true;
	}

	public void increaseHeight(int x, int y, int height) {
		this.setHeight(x, y, this.getHeight(x, y) + height);
	}

	public void setChunkGridPosition(int chunkCellX, int chunkCellY) {
		this.cellPosition = new CellPosition(chunkCellX, chunkCellY);
		this.worldXY = new CellPosition(this.worldProperties.chunkSize * chunkCellX,
				this.worldProperties.chunkSize * chunkCellY);
	}

	public CellPosition setChunkGridPosition() {
		return this.cellPosition;
	}

	public void setFromWorldData(WorldData worldData) {
		this.worldData = worldData;
		worldData.clearCache();
		for (int y = 0; y < this.sizePlusMargin; y++) {
			for (int x = 0; x < this.sizePlusMargin; x++) {
				this.setHeight(x, y, worldData.getHeight(x - this.worldXY.x, y + this.worldXY.y));
			}
		}
	}

	public WorldProperties getWorldProperties() {
		return this.worldProperties;
	}

	public CellPosition getCellPosition() {
		return this.cellPosition;
	}

	@Override
	public int compareTo(Chunk o) {
		return Integer.compare(this.cellPosition.y, o.cellPosition.y);
	}
}
