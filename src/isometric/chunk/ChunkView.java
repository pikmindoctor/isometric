package isometric.chunk;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import isometric.entities.EntityProperties;
import isometric.sidesAndVectors.CellPosition;
import isometric.sidesAndVectors.Vector2;
import isometric.terrain.TerrainCellImageManager;
import isometric.world.WorldProperties;

public class ChunkView implements Comparable<ChunkView> {
	private BufferedImage img;
	private WorldProperties worldProperties;
	private int imgWidth;
	private int imgHeight;
	private Chunk chunk;
	private boolean drawn = false;
	private boolean drawStarted = false;
	private TerrainCellImageManager cellImages;
	private CellPosition cellPosition = new CellPosition(0, 0);
	private Vector2 pixelPosition;

	public ChunkView(WorldProperties worldProperties, TerrainCellImageManager cellImages, Chunk chunk) {
		this.worldProperties = worldProperties;
		this.cellImages = cellImages;
		this.imgWidth = this.worldProperties.tilePixelWidth
				* (this.worldProperties.chunkSize + this.worldProperties.chunkMargin * 2);
		this.imgHeight = (int) (this.worldProperties.tileFlatPixelHeight
				* (this.worldProperties.chunkSize + this.worldProperties.chunkMargin * 2)
				+ this.worldProperties.tileElevatedPixelHeight);
		this.img = new BufferedImage(this.imgWidth, this.imgHeight, BufferedImage.TYPE_INT_ARGB);
		this.chunk = chunk;
		CellPosition cellPosition = chunk.setChunkGridPosition();
		this.cellPosition.x = cellPosition.x;
		this.cellPosition.y = cellPosition.y;
		int wo2 = (int) (this.worldProperties.tilePixelWidth * this.worldProperties.chunkSize / 2.0);
		int ho2 = (int) (this.worldProperties.tileFlatPixelHeight * this.worldProperties.chunkSize / 2.0);

		this.pixelPosition = new Vector2(wo2 * this.cellPosition.x + 2 * ho2 * this.cellPosition.y,
				wo2 * this.cellPosition.x / 2 - ho2 * this.cellPosition.y);
	}

	public CellPosition getCellPosition() {
		return this.cellPosition;
	}

	public Vector2 getPixelPosition() {
		return this.pixelPosition;
	}

	public void drawLater() {
		if (!this.drawStarted) {
			if (this.worldProperties.concurrentChunkDrawing) {
				this.drawStarted = true;
				draw();
			} else {
				this.drawStarted = true;
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						draw();
					}
				});
				t.start();
			}
		}
	}

	public void draw() {
		this.drawStarted = true;
		Graphics2D g2d = (Graphics2D) this.img.getGraphics();
		g2d.setBackground(new Color(0, 0, 0, 0));
		g2d.clearRect(0, 0, this.imgWidth, this.imgHeight);
		for (int layer = 0; layer < this.worldProperties.nLayers; layer++) {
			this.drawLayer(g2d, 0, layer > 0, layer);
		}
		this.drawEntities(g2d);
		this.drawn = true;
	}

	private void drawLayer(Graphics2D g2d, double layerHeight, boolean transparentFloor, int layer) {
		int tileWidth = this.worldProperties.tilePixelWidth;
		int tileHeight = this.worldProperties.tileFlatPixelHeight;
		int chunkSize = this.worldProperties.chunkSize;
		int chunkSizeO2 = chunkSize / 2;
		int margin = this.worldProperties.chunkMargin;
		int marginX2 = margin * 2;
		int fullSize = chunkSize + marginX2;
		double targetX;
		double targetY;
		for (int y = fullSize - 1 - margin; y >= margin; y--) {
			for (int x = fullSize - 1 - margin; x >= margin; x--) {
				targetX = chunkSizeO2 - 1 - (x - marginX2) / 2.0 + (y - marginX2) / 2.0;
				targetY = chunkSizeO2 - 1 - (x - marginX2) / 2.0 - (y - marginX2) / 2.0;
				g2d.drawImage(
						this.cellImages
								.getCellTerrain(this.chunk.getCellTerrainProperties(x, y, layer, transparentFloor)),
						(int) ((targetX + 1) * tileWidth),
						(int) (targetY * tileHeight - layerHeight) + this.imgHeight / 2, null);
			}
		}
	}

	private void drawEntities(Graphics2D g2d) {
		EntityProperties entityProperties;
		int tileWidth = this.worldProperties.tilePixelWidth;
		int tileHeight = this.worldProperties.tileFlatPixelHeight;
		int margin = this.worldProperties.chunkMargin;
		int marginX2 = margin * 2;
		int chunkSize = this.worldProperties.chunkSize;
		int chunkSizeO2 = chunkSize / 2;
		int fullSize = chunkSize + marginX2;
		double targetX;
		double targetY;
		double _x;
		double _y;
		CellPosition offset;
		for (int y = fullSize - 1 - margin; y >= margin; y--) {
			for (int x = fullSize - 1 - margin; x >= margin; x--) {
				entityProperties = this.chunk.getEntityProperties(x, y);
				if (entityProperties != null) {
					offset = entityProperties.getOffset();
					_x = x + (double) offset.x / (double) chunkSize;
					_y = y + (double) offset.y / (double) chunkSizeO2;
					targetX = chunkSizeO2 - 1 - (_x - marginX2) / 2.0 + (_y - marginX2) / 2.0;
					targetY = chunkSizeO2 - 1 - (_x - marginX2) / 2.0 - (_y - marginX2) / 2.0;
					g2d.drawImage(entityProperties.getTexture(), (int) ((targetX + 1) * tileWidth),
							(int) (targetY * tileHeight) + this.imgHeight / 2, null);
				}
			}
		}
	}

	public boolean getDrawn() {
		return this.drawn;
	}

	public BufferedImage getImage() {
		return this.img;
	}

	@Override
	public int compareTo(ChunkView o) {
		return Double.compare(this.pixelPosition.y, o.pixelPosition.y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pixelPosition == null) ? 0 : pixelPosition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChunkView other = (ChunkView) obj;
		if (pixelPosition == null) {
			if (other.pixelPosition != null)
				return false;
		} else if (!pixelPosition.equals(other.pixelPosition))
			return false;
		return true;
	}

	public boolean getDrawStarted() {
		return this.drawStarted;
	}

}
