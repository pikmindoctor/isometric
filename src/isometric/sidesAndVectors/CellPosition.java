package isometric.sidesAndVectors;

public class CellPosition implements Comparable<CellPosition> {
	public int x;
	public int y;

	public CellPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "x" + x + ", y" + y;
	}

	@Override
	public int compareTo(CellPosition cellPosition) {
		int result = Integer.compare(this.y, cellPosition.y);
		if (result == 0) {
			return Integer.compare(this.x, cellPosition.x);
		} else {
			return result;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CellPosition other = (CellPosition) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
}
