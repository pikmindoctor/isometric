package isometric.sidesAndVectors;

public class Vector2i {
	public int x;
	public int y;

	public Vector2i(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "x" + x + ", y" + y;
	}
}
