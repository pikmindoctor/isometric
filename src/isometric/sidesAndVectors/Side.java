package isometric.sidesAndVectors;

public enum Side {
	east, northEast, north, northWest, west, southWest, south, southEast
}
