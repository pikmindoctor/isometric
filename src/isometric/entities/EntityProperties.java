package isometric.entities;

import java.awt.image.BufferedImage;

import isometric.images.ImageLibrary;
import isometric.sidesAndVectors.CellPosition;

public class EntityProperties {
	private String typeName;
	private String textureName;
	private BufferedImage texture;
	private CellPosition offset;

	public EntityProperties(String typeName, String textureName, int offsetX, int offsetY) {
		this.typeName = typeName;
		this.textureName = textureName;
		this.offset = new CellPosition(offsetX, offsetY);
	}

	public String getID() {
		return this.typeName + ":" + this.offset.x + "," + this.offset.y;
	}

	public BufferedImage getTexture() {
		if (this.texture == null) {
			this.texture = ImageLibrary.getImage(this.textureName, true);
		}
		return this.texture;
	}

	public CellPosition getOffset() {
		return this.offset;
	}

	public String getTypeName() {
		return this.typeName;
	}
}
