package isometric.entities;

import java.util.HashMap;
import java.util.Random;

import isometric.world.WorldProperties;

public class EntityPropertiesManager {
	private HashMap<String, EntityProperties> map = new HashMap<String, EntityProperties>();
	private int nTreeOffsetVariations = 32;
	private WorldProperties worldProperties;

	public EntityPropertiesManager(WorldProperties worldProperties) {
		this.worldProperties = worldProperties;
	}

	public void addEntityProperties(EntityProperties entityProperties) {
		this.map.put(entityProperties.getTypeName(), entityProperties);
	}

	public EntityProperties getEntityProperties(String typeName) {
		return this.map.get(typeName);
	}

	public void generateDefaultEntities() {
		Random random = new Random();
		int spreadX = worldProperties.tilePixelWidth;
		int spreadY = worldProperties.tileFlatPixelHeight;
		for (int a = 0; a < this.nTreeOffsetVariations; a++) {
			this.addEntityProperties(new EntityProperties("Tree" + a, "Tree.png", random.nextInt(spreadX) - spreadX / 2,
					random.nextInt(spreadY) - spreadY / 2));

			// this.addEntityProperties(new EntityProperties("Tree" + a, "Tree.png", 0, 0));
		}
	}

	public int getNTreeOffsetVariations() {
		return this.nTreeOffsetVariations;
	}
}
