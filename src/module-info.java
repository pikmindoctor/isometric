module isometric
{
    exports isometric.chunk;
    exports isometric.images;
    exports isometric.util;
    exports isometric.entities;
    exports isometric.app;
    exports isometric.sidesAndVectors;
    exports isometric.world;
    exports isometric.terrain;

    requires java.desktop;
}